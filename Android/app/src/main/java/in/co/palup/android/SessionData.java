package in.co.palup.android;

/**
 * Keys for data to be shared across activities
 * <p>
 * Created by kuldeep on 06/12/15.
 */
public enum SessionData {
    NAME, EMAIL, LOCATION, PHOTO_URL
}
