package in.co.palup.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.palup.entity.UserCreationDetails;

import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.SubmitRequest;

/**
 * Login
 */
public class Login extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {

    /**
     * Signin request code to identify response
     */
    private static final int RC_SIGN_IN = 9001;

    /**
     * Log tag
     */
    private static final String TAG = "Login";

    /**
     * Google API client to handle sign in
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Configuration handler
     */
    private Configuration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.configuration = new Configuration(getApplicationContext());

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setColorScheme(SignInButton.COLOR_DARK);
        signInButton.setScopes(gso.getScopeArray());

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    /**
     * Sign in
     */
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /**
     * Handle signin result
     *
     * @param result containing account info
     */
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount account = result.getSignInAccount();

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SessionData.class.getName(), 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(SessionData.EMAIL.name(), account.getEmail());
            editor.putString(SessionData.NAME.name(), account.getDisplayName());
            Log.i(TAG, account.getPhotoUrl().toString());
            editor.putString(SessionData.PHOTO_URL.name(), account.getPhotoUrl().toString());
            editor.apply();

            // TODO: abort if authentication fails and handle exception gracefully
            update(account);
        } else {
            // Signed out, show unauthenticated UI
            Log.i(TAG, "Login failed");
            Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param account for authenticated user
     */
    private void update(GoogleSignInAccount account) {

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
                Toast.makeText(getApplicationContext(), "login failed", Toast.LENGTH_SHORT).show();
            }
        };

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, response);
                Intent homeActivity = new Intent(getApplicationContext(), PalupHome.class);
                startActivity(homeActivity);
            }
        };

        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/user/update";
        final UserCreationDetails userCreationDetails =
                new UserCreationDetails(account.getDisplayName(), account.getEmail(), account.getPhotoUrl().toString());

        Gson gson = new Gson();
        SubmitRequest request = new SubmitRequest(url, gson.toJson(userCreationDetails), listener, errorListener);
        PalupSingleton.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "googleApiClient could not connect");
        Log.d(TAG, connectionResult.toString());
    }
}
