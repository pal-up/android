package in.co.palup.android.dialog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import org.joda.time.LocalDate;

import java.util.Calendar;

/**
 * Date picker
 * <p>
 * Created by Kuldeep Yadav on 25-Nov-15.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    /**
     * Listener to respond on complete event
     */
    private OnCompleteListener onCompleteListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int monthOfYear = c.get(Calendar.MONTH);
        int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it
        // Note: In java.util.Calendar,
        // monthOfYear starts from 0 ends in 11,
        // but DatePickerDialog accepts in [1,12]
        // so 1 is added to month explicitly
        return new DatePickerDialog(getActivity(), this, year, monthOfYear + 1, dayOfMonth);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.onCompleteListener = (OnCompleteListener) activity;
    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        // Note: In java.util.Calendar, monthOfYear starts from 0 ends in 11,
        // but DatePickerDialog accepts in [1,12] so 1 is added to month explicitly
        LocalDate date = new LocalDate(year, monthOfYear + 1, dayOfMonth);
        onCompleteListener.onComplete(date);
    }

    /**
     * Listener for on complete event
     */
    public interface OnCompleteListener {

        /**
         * Invoke when event completes
         *
         * @param date set in picker
         */
        void onComplete(LocalDate date);
    }
}
