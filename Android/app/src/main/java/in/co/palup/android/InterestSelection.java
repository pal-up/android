package in.co.palup.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.palup.entity.Interest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.GsonRequest;
import in.co.palup.android.volley.SubmitRequest;

/**
 * InterestSelection of User
 */
public class InterestSelection extends AppCompatActivity {

    /**
     * Logger tag
     */
    private final String TAG = "Interest Selection";

    /**
     * List of all labels
     */
    private List<Interest> labels;

    /**
     * Selection
     */
    private Set<Integer> selection;

    /**
     * Configuration handler
     */
    private Configuration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest);

        this.configuration = new Configuration(getApplicationContext());
        fetchLabels();
        this.selection = new HashSet<>();
    }

    /**
     * Fetch labels from server
     */
    private void fetchLabels() {

        String email = getSharedPreferences(SessionData.class.getName(), 0).getString(SessionData.EMAIL.name(), null);
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + ("/user/interest/get/" + email);

        Response.Listener<List<Interest>> listener = new Response.Listener<List<Interest>>() {
            @Override
            public void onResponse(List<Interest> response) {
                labels = response;
                ArrayAdapter<Interest> adapter = new ArrayAdapter<>(InterestSelection.this, android.R.layout.simple_list_item_multiple_choice, response);
                final ListView interestView = (ListView) findViewById(R.id.interest_list);
                interestView.setAdapter(adapter);

                for (int i = 0; i < response.size(); i++) {
                    interestView.setItemChecked(i, response.get(i).isInterested());
                    if (response.get(i).isInterested()) {
                        selection.add(i);
                    }
                }

                interestView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (interestView.isItemChecked(position)) {
                            selection.add(position);
                        } else {
                            selection.remove(position);
                        }
                    }
                });
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
            }
        };

        GsonRequest<List<Interest>> request = new GsonRequest<>(url, null,
                new TypeToken<List<Interest>>() {
                }.getType(), listener, errorListener);
        PalupSingleton.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onStop() {
        super.onStop();
        submitLabels();
    }

    /**
     * Submit selected labels to server
     */
    private void submitLabels() {

        Set<String> selectedLabels = new HashSet<>();
        for (int index : selection) {
            selectedLabels.add(labels.get(index).getLabel().getId());
        }

        String email = getSharedPreferences(SessionData.class.getName(), 0).getString(SessionData.EMAIL.name(), null);
        String url = configuration.getScheme() + "://" + configuration.getAuthority() + ("/user/interest/set/" + email);

        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, response);
                Toast.makeText(InterestSelection.this, "Interest submitted", Toast.LENGTH_SHORT).show();
                Intent intentHome = new Intent(getApplicationContext(), PalupHome.class);
                startActivity(intentHome);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
            }
        };

        Gson gson = new Gson();
        SubmitRequest request = new SubmitRequest(url, gson.toJson(selectedLabels), listener, errorListener);
        PalupSingleton.getInstance(this).getRequestQueue().add(request);
    }

}
